import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

class LoginPage extends React.Component {



    toggleModal(divId) {
        if (divId == 'login') {
            $("#" + divId).show();
            $("#" + 'signup').hide();
        }
        if (divId == 'signup') {
            $("#" + divId).show();
            $("#" + 'login').hide();
        }
    }

    closeModal(divId) {
        $("#" + divId).hide()
    }



    render() {

        return (
            <Fragment>

                {/* loginModal */}
                <div className="slider-right hidden" id="login">
                    <button className="close-btn" onClick={() => this.closeModal('login')}><i><img src="images/close-icon.png" /></i></button>
                    <div className="login-part">
                        <h1>Login</h1>
                        <p>or <a href="#" onClick={() => this.toggleModal('signup')}>create an account</a></p>
                        <input type="text" id="" name="" placeholder="Phone Number" className="input-box" />
                        <button className="Submit-btn" onClick="window.location.href='index.html'">Login</button>
                    </div>
                </div>
                {/* loginModal */}


                {/* signupModal */}
                <div className="slider-right hidden" id="signup">
                    <button className="close-btn" onClick={() => this.closeModal('signup')}><i><img src="images/close-icon.png" /></i></button>
                    <div className="login-part">
                        <h1>Sign Up</h1>
                        <p>or <a href="#" onClick={() => this.toggleModal('login')}>login to your account</a></p>
                        <input type="text" id="" name="" placeholder="Phone Number" className="input-box" />
                        <input type="text" id="" name="" placeholder="Name " className="input-box" />
                        <input type="text" id="" name="" placeholder="Email" className="input-box" />
                        <input type="text" id="" name="" placeholder="Password" className="input-box" />
                        <input type="text" id="" name="" placeholder="Referral Code" className="input-box" />
                        <button className="Submit-btn">Continue</button>
                        <p className="mt-2">By creating an account, I accept the <a href="#">Terms & Conditions</a></p>
                    </div>
                </div>
                {/* signupModal */}


                {/* headerSection */}
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xl-6 col-lg-12">
                            <div className="login-location">
                                <div className="login-header">
                                    <div className="row">
                                        <div className="col-4">
                                            <div className="logo"><img src="images/logo.png" /></div>
                                        </div>
                                        <div className="col-8">
                                            <ul className="login-nav">
                                                <li><a href="#" onClick={() => this.toggleModal('login')}>Login</a></li>
                                                <li className="signup-btn"><a href="#" onClick={() => this.toggleModal('signup')}>Signup</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="heading-text">
                                    <h1>Hungry?</h1>
                                    <p>Order Food from favourite restaurants near you.</p>
                                </div>
                                <div className="search-location-part">
                                    <input type="text" name="" id="" placeholder="Enter your delivery location" className="location-input" />
                                    <input type="text" name="" id="" placeholder="Locat Me" className="location-input2" />
                                    <button className="btn find-food-btn">Find Food</button>
                                </div>
                                <h3 className="city-head">Popular cities in India</h3>
                                <ul className="city-name">
                                    <li><a href="#">Ahmedabad</a></li>
                                    <li><a href="#">Bangalore</a></li>
                                    <li><a href="#">Chennai</a></li>
                                    <li><a href="#">Delhi</a></li>
                                    <li><a href="#">Gurgaon</a></li>
                                    <li><a href="#">Hyderabad</a></li>
                                    <li><a href="#">Kolkata</a></li>
                                    <li><a href="#">Mumbai</a></li>
                                    <li><a href="#">Pune</a></li>
                                    <li><a href="#">& more.</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-12">
                            <div className="login-header-img"> <img src="images/login-img.jpg" /> </div>
                        </div>
                    </div>
                </div>
                {/* headerSection */}


                {/* orderSection */}

                <section className="howToProcess">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-4 col-md-4 col-sm-12">
                                <div className="process-box"> <i><img src="images/No_min_order.png" /></i>
                                    <h3>No Minimum Order</h3>
                                    <p>Order in for yourself or for the group, with no restrictions on order value</p>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-4 col-sm-12">
                                <div className="process-box"> <i><img src="images/Live_order.png" /></i>
                                    <h3>Live Order Tracking</h3>
                                    <p>Know where your order is at all times, from the restaurant to your doorstep</p>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-4 col-sm-12">
                                <div className="process-box"> <i><img src="images/Live_order.png" /></i>
                                    <h3>Lightning-Fast Delivery</h3>
                                    <p>Experience Br Food's superfast delivery for food delivered fresh & on time</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="appPart">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 col-sm-12">
                                <div className="app-content">
                                    <h1>Restaurants in your pocket</h1>
                                    <p>Order from your favorite restaurants & track on the go, with the all-new Br Food app.</p>
                                    <div className="mobile-app"> <a href="#"><img src="images/play_icon.webp" /></a>
                                        <a href="#"><img src="images/iOS_icon.webp" /></a> </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-sm-12">
                                <div className="app-img"> <img src="images/app-img.png" /> </div>
                            </div>
                        </div>
                    </div>
                </section>

                {/* orderSection */}


                {/* footerSection */}
                <footer>
                    <div className="container">
                        <div className="row footer-col1">
                            <div className="col-sm-3">
                                <h3>COMPANY</h3>
                                <ul>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">Team</a></li>
                                    <li><a href="#">Careers</a></li>
                                    <li><a href="#">BR Food Blog</a></li>
                                    <li><a href="#">Bug Bounty</a></li>
                                    <li><a href="#">BR Food Pop</a></li>
                                    <li><a href="#">BR Food</a></li>
                                </ul>
                            </div>
                            <div className="col-sm-3">
                                <h3>CONTACT</h3>
                                <ul>
                                    <li><a href="#">Help & Support</a></li>
                                    <li><a href="#">Partner with us</a></li>
                                    <li><a href="#">Ride with us</a></li>
                                </ul>
                            </div>
                            <div className="col-sm-3">
                                <h3>LEGAL</h3>
                                <ul>
                                    <li><a href="#">Terms & Conditions</a></li>
                                    <li><a href="#">Refund & Cancellation</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Cookie Policy</a></li>
                                    <li><a href="#">Offer Terms</a></li>
                                </ul>
                            </div>
                            <div className="col-sm-3">
                                <div className="footer-app-icon">
                                    <div className="mobile-app"> <a href="#"><img src="images/play_icon.webp" /></a>
                                        <a href="#"><img src="images/iOS_icon.webp" /></a> </div>
                                </div>
                            </div>
                        </div>
                        <div className="row footer-col1">
                            <div className="col-sm-12 mb-2">
                                <h3>WE DELIVER TO</h3>
                            </div>
                            <div className="col-sm-3">
                                <ul>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">Team</a></li>
                                    <li><a href="#">Careers</a></li>
                                    <li><a href="#">BR Food Blog</a></li>
                                    <li><a href="#">Bug Bounty</a></li>
                                    <li><a href="#">BR Food Pop</a></li>
                                    <li><a href="#">BR Food</a></li>
                                </ul>
                            </div>
                            <div className="col-sm-3">
                                <ul>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">Team</a></li>
                                    <li><a href="#">Careers</a></li>
                                    <li><a href="#">BR Food Blog</a></li>
                                    <li><a href="#">Bug Bounty</a></li>
                                    <li><a href="#">BR Food Pop</a></li>
                                    <li><a href="#">BR Food</a></li>
                                </ul>
                            </div>
                            <div className="col-sm-3">
                                <ul>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">Team</a></li>
                                    <li><a href="#">Careers</a></li>
                                    <li><a href="#">BR Food Blog</a></li>
                                    <li><a href="#">Bug Bounty</a></li>
                                    <li><a href="#">BR Food Pop</a></li>
                                    <li><a href="#">BR Food</a></li>
                                </ul>
                            </div>
                            <div className="col-sm-3">
                                <ul>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">Team</a></li>
                                    <li><a href="#">Careers</a></li>
                                    <li><a href="#">BR Food Blog</a></li>
                                    <li><a href="#">Bug Bounty</a></li>
                                    <li><a href="#">BR Food Pop</a></li>
                                    <li><a href="#">BR Food</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="copyright">
                            <div className="row">
                                <div className="col-sm-12 text-center">
                                    <p>&copy; Copyright 2019. BR FOOOD.</p>
                                    <ul>
                                        <li><a href="#"><i className="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i className="fa fa-instagram"></i></a></li>
                                        <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                {/* footerSection */}

            </Fragment>
        );
    }
}

function mapStateToProps(state) {


}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage }; 